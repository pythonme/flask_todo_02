from flask import render_template, request, redirect, url_for, jsonify
from app import app
from app.models import Todo
from app import db



@app.route('/login')
def login():
	return render_template('login.html')

@app.route('/')
def index():
	result = Todo.query.all() 
	return render_template('index.html', result=result)

@app.route('/status/<state>')
def status(state):
    status = "Todo"
    if state == "progress":
        status = "In Progress"
    elif state == "complete":
        status = "Complete"

    result = Todo.query.filter(Todo.status == status)
    return render_template('index.html', result=result)

@app.route('/add', methods=['POST'])
def add():
    data = request.get_json()
    todo = Todo(name=data['name'],status='Todo')
    db.session.add(todo)
    db.session.commit()
    return render_template('index.html')

@app.route('/edit/<id>', methods=['POST'])
def edit(id):
    data = request.get_json()
    todo = db.session.query(Todo).get(id)

    if "status" in data:
       todo.status = data['status']
    elif "name" in data:
       # First way to update
       #db.session.query(Todo).filter_by(id=id).update({"name":data['name']})
       
       # Second way to update
       #todo = db.session.query(Todo).get(id)
       todo.name = data['name']
    else:
       print("Nothing to upate")
        
    db.session.commit()
    return render_template('index.html')


@app.route('/remove/<id>', methods=['POST'])
def remove(id):
	# The following line doesn't work because it will create a new object
	# todo = Todo(id=id)

	# We need to query the existing object in DB that we want to delete
    todo = Todo.query.filter_by(id=id).first()
    db.session.delete(todo)
    db.session.commit()
    return render_template('index.html')
