from app import db

class Todo(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(200))
	status = db.Column(db.String(200))

	def __repr__(self):
		return self.text
