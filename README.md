# flask_todo_02


## Getting started

### Create sqlite db

```bash
cd flask_todo_02
sudo apt-get install sqlite
sqlite3 todo.db
```

```sql
CREATE TABLE todo (
	id INTEGER PRIMARY KEY,
	name TEXT NOT NULL,
	status TEXT NOT NULL
);
```

### Install libs and run
```bash
cd flask_todo_02

# First time install python and pip packages via pipenv
pipenv shell
pipenv install


# Run
python main.py
```


